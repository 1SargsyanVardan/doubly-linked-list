#include <iostream>
#include <vector>


template <typename T>
class DoublyLinkedList {
	struct Node {
		T value;
		Node* next;
		Node* prev;
		Node():
			value(0)
			,next(nullptr)
			,prev(nullptr)
		{ }
		Node(const T& value,Node* next,Node* prev):
			value(value)
			,next(next)
			,prev(prev)
		{ }
		~Node()
		{
			std::cout << "Node Destructor\n";
		}
	};
	Node* head;
	Node* tail;
	std::size_t list_size{};

public:
	DoublyLinkedList() :
		head(nullptr)
		,tail(nullptr)
		,list_size(0) {
		std::cout << "Default\n";
	}
	DoublyLinkedList(const DoublyLinkedList& other):
		head(other->head)
		,tail(other->tail)
		,list_size(other.size())
	{
		//?
	}
	~DoublyLinkedList() {
		std::cout << "Destructor\n";
		while (head != nullptr)
		{
			Node* current = head;
			head = head->next;
			delete current;
		}
	}
	void push_front(const T& value) {
		if (list_size == 0)
		{
			Node* newNode = new Node(value, nullptr, nullptr);
			head = newNode;
			++list_size;
			delete newNode;
			return;
		}
		Node* newNode = new Node(value, head, nullptr);
		head = newNode;
		++list_size;
	}
	void push_back(const T& value) {
		if (list_size == 0)
		{
			Node* newNode = new Node(value, nullptr, nullptr);
			head = newNode;
			++list_size;
			delete newNode;
			return;
		}
		Node* newNode = new Node(value, nullptr, tail);
		tail = newNode;
		++list_size;
	}
	void pop_back() {
		if(list_size==0)
			throw std::exception{ "List size is 0, you can't do pop_back" };
		Node* ptr = tail;
		tail = tail->prev;
		tail->next = nullptr;
		delete ptr;
		--list_size;
		//delete next
	}
	std::size_t size() const
	{
		return list_size;
	}
	friend std::ostream& operator<<(std::ostream& out, const DoublyLinkedList& sll)
	{
		Node* current = sll.head;
		while (current != nullptr)
		{
			out << current->value << " ";
			current = current->next;
		}

		return out;
	}
};
int main()
{
	DoublyLinkedList<int> list;
	list.push_back(10);
	list.push_back(10);
	list.push_back(10);
	std::cout << list;
	return 0;
}
